import axios from 'axios'; // lets us perform ajax requests

const API_KEY = "0413ce75395494958f5bbc514ca9208a"
//${} <- cleans up the code a bit instead of doing + API_KEY
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER'
// Action creaters always have to return an action
// THEY ALWAYS HAVE TO HAVE A TYPE
export function fetchWeather(city){    
    const url = `${ROOT_URL}&q=${city},us`;
    const request = axios.get(url); // take url and make a get request which returns a promise    

    return {
        type: FETCH_WEATHER,
        // if the payload is a promise redux-promise stops the action entirely
        // and then once the request finishes, it dispatches a new action of the same type (FETCH_WEATHER)
        // so it unwraps the promise. Waits until the promise resolves
        // then sends the resolved promise
        payload: request
    };
}