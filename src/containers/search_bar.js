import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import {fetchWeather } from '../actions/index';

class SearchBar extends Component {

    constructor(props) {
        super(props);

        this.state = {term: ''};

        // allows the line this.setState({term: event.target.value}); to not crash. 
        // Properly directs(?) the "this" value. It doesn't know what this.setState is without this
        // It binds the context
        // I think that means "this" refers to the class than instead of the callback function
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(event){        
        this.setState({term: event.target.value});
    }

    // this tells the browser to not  submit the form (lul)
    // comments in return section explain why we do this
    onFormSubmit(event){        
        event.preventDefault();

        // Need to fetch weather data
        this.props.fetchWeather(this.state.term);
        // clear searchBar
        this.setState({term: ''});

        
    }

    render(){
        return (
            // html here is coming from bootstrap which makes the css easy            
            // when pressing enter inside a form. The browser automatically thinks you are trying to submit the 
            // contents of this form
            // It then makes a post request to the server. This is a normal html thing
            // We can change this behaviour by adding the event handler onSubmit={this.onFormSubmit}
            // Still makes sense to use a form because of what users expect to happen. We also get some free functionality
            // Like pressing enter or pressing submit submits form (the searchbar)
            // Cuts down on the number of cases we have to code for (enter vs pressing submit button)            
            <form onSubmit={this.onFormSubmit} className="input-group">
                <input 
                    placeholder="Get a 5-day forecast in your favourite cities"
                    className="form-control"
                    value={this.state.term}
                    // whenever something changes run the function onInputChange
                    onChange={this.onInputChange} 
                /> 
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">Submit</button>
                </span>
            </form>

        );
    }
}

// causes the action creater "fetchWeather", makes sure it flows into the middleware and then the reducers
// This give us access to this.props.fetchWeather
function mapDispatchToProps(dispatch){
    return bindActionCreators({ fetchWeather}, dispatch);
}

// whenever you pass in a function it has to go as the second argument
// this container doesn't care about the state so just pass in null
export default connect(null, mapDispatchToProps)(SearchBar);