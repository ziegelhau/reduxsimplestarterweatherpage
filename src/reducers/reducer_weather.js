import { FETCH_WEATHER } from "../actions";

export default function(state = [], action){ 

    switch(action.type){
    case FETCH_WEATHER:
    // this is very bad return state.push(action.payload.data);
    // because we never want to update state directly
    // here we are mutating the array in place
    // need to return a completely new instance of state
    // concat returns a new array so this works
    // return state.concat([action.payload.data]); <- below is equivalent
    // the ... takes all the entries inside the array and 'flatten' it out.
    // makes [city, city, city] instead of [city, [city,city]]
        return [action.payload.data, ...state]; // unique to the api (payload.data)
    }
        
    return state;
}